If you want the website source, there are two ways:

1. git clone with "git clone https://gitlab.com/aneeshlingala/aneeshlingala.gitlab.io"

2. Going to "https://gitlab.com/aneeshlingala/aneeshlingala.gitlab.io" and click on the "Code" button, after that, then click on "Download ZIP", then extract the ZIP file, and there you go.
